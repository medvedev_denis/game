package chap02;

/**
 * Класс служит для запуска игры.
 */

public class GameLauncher {
    public static void main (String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}
