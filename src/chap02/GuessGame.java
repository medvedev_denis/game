package chap02;

/**
 * В данном классе находится логическая составляющая игры.
 */

public class GuessGame {

   
   public void startGame() {

       Player firstPlayer = new Player();
       Player secondPlayer = new Player();
       Player thirdPlayer = new Player();
       int firstPlayerGuessed = 0;
       int secondPlayerGuessed = 0;
       int thirdPlayerGuessed = 0;
       boolean firstPlayerIsRight = false;
       boolean secondPlayerIsRight = false;
       boolean thirdPlayerIsRight = false;

       int targetNumber = (int) (Math.random() * 10);
       System.out.println("Я думаю о числе между 0 и 9...");
       while(true) {
           System.out.println("Угадываемое число " + targetNumber);
           
           firstPlayer.guess();
           secondPlayer.guess();
           thirdPlayer.guess();

           System.out.println("Первый игрок предположил число " + firstPlayer.number);
           System.out.println("Второй игрок предположил число " + secondPlayer.number);
           System.out.println("Третий игрок предположил число " + thirdPlayer.number);
           
           if (firstPlayer.number == targetNumber) {
               firstPlayerIsRight = true;
           }
           if (secondPlayer.number == targetNumber) {
               secondPlayerIsRight = true;
           }
           if (thirdPlayer.number == targetNumber) {
               thirdPlayerIsRight = true;
           }
           
           if (firstPlayerIsRight || secondPlayerIsRight || thirdPlayerIsRight)
           {
               System.out.println("У нас есть победитель!");
               System.out.println("Первый игрок оказался прав? " + firstPlayerIsRight);
               System.out.println("Второй игрок оказался прав? " + secondPlayerIsRight);
               System.out.println("Третий игрок оказался прав? " + thirdPlayerIsRight);
               System.out.println("Игра окончена.");
               break;
           }
           else
           {
               System.out.println("Игрокам придется попробовать снова.");
           }
       }
   }
}
