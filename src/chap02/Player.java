package chap02;

/**
 * Класс описывает "параметры" игрока и вычисления, требуемые для него в ходе игры.
 */

public class Player {

    int number = 0;

    public void guess()
    {
        number = (int) (Math.random() * 10);
        System.out.println("Я думаю это " + number);
    }
}
